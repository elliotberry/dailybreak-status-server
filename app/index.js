//
const express = require('express')
//const bodyParser = require('body-parser')
const db = require("./db.js")
const app = express()
const port = 3000

app.get('/api', async (req, res) => {
    let reports = await db.findAll();
  res.send(reports)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

app.use('/', express.static('public'))
