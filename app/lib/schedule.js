const Bree = require('bree');
const Graceful = require('@ladjs/graceful');


//const logger = new Cabin();
 
const bree = new Bree({
  //logger,
  jobs: [
    {
      // runs `./jobs/email.js` on start and every minute
      name: 'isUp',
      interval: '1m',
      path: './reachable.js'
    }
  ]
});

const graceful = new Graceful({ brees: [bree] });
graceful.listen();
 
// start all jobs (this is the equivalent of reloading a crontab):
bree.start();