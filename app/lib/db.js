const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");
const adapter = new FileSync("./db.json");
const db = low(adapter);
const shortid = require("shortid");

db.defaults({
    reports: [],
}).write();

const getTime = () => {
    let y = new Date();
    return y;
}

module.exports = {

        findByID: async function(id) {
            try {
                let y = await db.get("reports").find({
                    id: id
                }).value();
                return y;
            } catch (err) {
                return {
                    err: true,
                    message: err
                };
            }
        },
        findAll: async function() {
            let y = await db.get("reports").value();
            return y;
        },
        write: async function(obj) {
            try {
                obj.id = shortid.generate();
                obj.time = getTime();
                let y = await db.get("reports").push(obj).write();
                return {
                    err: null,
                    message: obj
                };
            } catch (err) {
                return {
                    err: true,
                    message: err
                };
            }
        }
    
};
