
const lighthouse = require('lighthouse');
const chromeLauncher = require('chrome-launcher');

const runLighthouse = async(url) => {
  const chrome = await chromeLauncher.launch({chromeFlags: ['--headless']});
  const options = {logLevel: 'info', output: 'html', onlyCategories: ['performance'], port: chrome.port};
  const runnerResult = await lighthouse(url, options);

  await chrome.kill();
  return runnerResult;
}
module.exports = runLighthouse