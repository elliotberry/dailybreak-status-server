const isReachable = require("is-reachable");
const db = require("./db.js");
const urls = ["https://dailybreak.com"];

const isUp = async function () {
  for await (url of urls) {
    let r = await isReachable(urls[0]);
    let info = {
      type: "serverUp",
      url: urls[0],
      up: r
    };
    let u = await db.write(info)
    console.log(u)
  }

};
isUp();
